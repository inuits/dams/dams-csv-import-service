import app
import os
import pandas as pd

from io import StringIO


class Parser:
    filename_fields = ["bestandsnaam", "filename"]
    fields_to_ignore = ["_id", "_key", "asset_id", "object_id"] + filename_fields
    id_fields = ["_id", "_key", "asset_id", "object_id"] + filename_fields

    def __convert_csv_to_df(self, csv_content):
        return self.__read_csv(StringIO(csv_content))

    def __get_metadata_dict(self, key, value, lang="nl"):
        return {"key": key, "value": value, "lang": lang}

    def __read_csv(self, csv_file):
        df = None
        try:
            df = pd.read_csv(csv_file, delimiter=";").dropna(how="all")
        except Exception as ex:
            message = f"Reading .csv file {csv_file} failed with: {ex}"
            app.logger.error(message)
        return df

    def parse_csv_to_entities(self, csv_content):
        df = self.__convert_csv_to_df(csv_content)
        entities = dict()
        for index, row in df.iterrows():
            entity = None
            for id_field in self.id_fields:
                if id_field in row:
                    id = row[id_field]
                    entity = entities.get(
                        id,
                        {
                            "identifiers": [id]
                            if id_field not in self.filename_fields
                            else [],
                            "type": "asset",
                            "metadata": [],
                        },
                    )
                    entities[id] = entity
                    break
            if entity:
                for field, value in row.items():
                    if not pd.isna(value) and field not in self.fields_to_ignore:
                        entity["metadata"].append(
                            self.__get_metadata_dict(field, str(value))
                        )
        return list(entities.values())
