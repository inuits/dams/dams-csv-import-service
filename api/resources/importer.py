import app
import os

from cloudevents.conversion import to_dict
from cloudevents.http import CloudEvent
from flask import request
from flask_restful import abort, Resource
from inuits_jwt_auth.authorization import current_token


class ImporterStart(Resource):
    def __get_request_body(self):
        if request_body := request.get_json(silent=True):
            return request_body
        abort(405, message="Invalid input")

    @app.require_oauth("start-importer")
    def post(self):
        request_body = self.__get_request_body()
        attributes = {"type": "dams.import_requested", "source": "dams"}
        data = {
            "selected_folder": request_body["selected_folder"],
            "user": dict(current_token).get("email", "default_uploader"),
        }
        event = to_dict(CloudEvent(attributes, data))
        app.rabbit.send(event, routing_key="dams.import_requested")
        return event, 201


class ImporterDirectories(Resource):
    upload_source = os.getenv("UPLOAD_SOURCE", "/mnt/media-import")

    def __has_subdirs(self, path):
        with os.scandir(path) as entries:
            return any(entry.is_dir() for entry in entries)

    @app.require_oauth("get-importer-directories")
    def get(self):
        path = self.upload_source
        if request_dir := request.args.get("dir"):
            path = os.path.join(self.upload_source, request_dir.removeprefix("/"))
        if not os.path.exists(path):
            abort(400, message=f"{path} not found")
        directories = []
        with os.scandir(path) as entries:
            for directory in [x for x in entries if x.is_dir()]:
                directories.append(
                    {
                        "dir": directory.path.removeprefix(self.upload_source),
                        "has_subdirs": self.__has_subdirs(directory.path),
                    }
                )
            return directories
