import app

from importer import Importer


@app.rabbit.queue("dams.import_requested")
def import_from_csv(routing_key, body, message_id):
    if any(x not in body["data"] for x in ["selected_folder", "user"]):
        return
    importer = Importer(body["data"]["selected_folder"], body["data"]["user"])
    importer.import_from_csv()
