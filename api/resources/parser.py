import app
import parser

from flask import request, Response
from flask_restful import Resource


class EntitiesParser(Resource):
    def post(self):
        return parser.Parser().parse_csv_to_entities(request.get_data(as_text=True))
