import app
import base64
import glob
import os
import pandas as pd
import requests

from job_helper.Status import Status


class DuplicateFileException(Exception):
    def __init__(self, message):
        super().__init__(message)


class Importer:
    def __init__(self, selected_folder, user=None):
        self.collection_api_url = os.getenv("COLLECTION_API_URL")
        self.upload_source = os.getenv("UPLOAD_SOURCE")
        self.selected_folder = os.path.join(
            self.upload_source, str(selected_folder).removeprefix("/")
        )
        self.user = user
        self.mapped_fields = [
            {"key": "Priref", "value": ""},
            {"key": "Objectnummer", "value": ""},
            {"key": "Bestandsnaam", "value": ""},
            {"key": "Padnaam", "value": ""},
            {"key": "Rechtenstatus", "value": "rights", "fallback": "In Copyright"},
            {"key": "Rechtenhouder", "value": "copyright"},
            {"key": "Fotograaf", "value": "photographer"},
            {"key": "Bron", "value": "source"},
            {
                "key": "Publicatiestatus",
                "value": "publication_status",
                "fallback": "niet-publiek",
            },
        ]
        self.headers = {"Authorization": f'Bearer {os.getenv("STATIC_JWT")}'}
        self.main_job = None
        self.sub_jobs = []
        self.sixth_collection_id = None

    def __add_institution_relation(self, object_id):
        if not self.sixth_collection_id:
            self.sixth_collection_id = requests.get(
                f"{self.collection_api_url}/entities/sixthcollection/entity_id",
                headers=self.headers,
            ).json()
        relations = [
            {
                "key": self.sixth_collection_id,
                "label": "MaterieelDing.beheerder",
                "type": "isIn",
                "value": "Collectie van de Gentenaar",
            }
        ]
        self.__post_request(
            f"{self.collection_api_url}/entities/{object_id}/relations", relations
        )

    def __check_malformed_row(self, row):
        if pd.isna(row["Priref"]):
            raise Exception("Row has no 'Priref'")
        elif pd.isna(row["Objectnummer"]):
            raise Exception("Row has no 'Objectnummer'")
        elif pd.isna(row["Padnaam"]) and pd.isna(row["Bestandsnaam"]):
            raise Exception("Both 'Padnaam' & 'Bestandsnaam' are not available in row")

    def __create_asset(self, priref, object_id, filename, row):
        stripped_object_id = object_id
        for prefix in [
            "archiefgent:",
            "dmg:",
            "hva:",
            "industriemuseum:",
            "stam:",
            "cogent:",
        ]:
            stripped_object_id = stripped_object_id.removeprefix(prefix)
        entity = {
            "identifiers": [row["Priref"], priref, object_id],
            "type": "asset",
            "metadata": [
                {"key": "title", "value": filename},
                {
                    "key": "object_number",
                    "label": "objectnummer",
                    "value": stripped_object_id,
                },
            ],
            "object_id": object_id,
        }
        self.__post_request(f"{self.collection_api_url}/entities", entity)
        # FIXME: add mapping for other institutions
        if object_id.startswith("cogent:CG_"):
            self.__add_institution_relation(object_id)

    def __create_mediafile(self, identifier, mediafile, raise_exception=True):
        return self.__post_request(
            f"{self.collection_api_url}/entities/{identifier}/mediafiles/create",
            mediafile,
            raise_exception=raise_exception,
        )

    def __get_metadata(self, row):
        new_metadata = []
        for elem in [elem for elem in self.mapped_fields if elem["value"]]:
            if not pd.isna(row[elem["key"]]):
                new_metadata.append({"key": elem["value"], "value": row[elem["key"]]})
            elif "fallback" in elem:
                new_metadata.append({"key": elem["value"], "value": elem["fallback"]})
        return new_metadata

    def __get_upload_location(self, priref, object_id, filename, row):
        mediafile = {"filename": filename, "metadata": self.__get_metadata(row)}
        ret = self.__create_mediafile(priref, mediafile, False)
        if ret.status_code == 201:
            return ret.json()
        ret = self.__create_mediafile(object_id, mediafile, False)
        if ret.status_code == 201:
            self.__update_asset(object_id, priref, row)
            return ret.json()
        self.__create_asset(priref, object_id, filename, row)
        return self.__create_mediafile(priref, mediafile).json()

    def __parse_path(self, row):
        if pd.isna(row["Padnaam"]):
            path_kind = "Bestandsnaam"
            found_paths = sorted(
                glob.glob(
                    f'{self.selected_folder}**/{row["Bestandsnaam"]}', recursive=True
                )
            )
            file_path = found_paths[0] if len(found_paths) else ""
        else:
            path_kind = "Padnaam"
            if row["Padnaam"][1] == ":":
                file_path = str.replace(row["Padnaam"][3:], "\\", "/")
            else:
                file_path = row["Padnaam"].strip("/")
            file_path = os.path.join(self.upload_source, file_path)
        if not file_path or not os.path.exists(file_path):
            raise Exception(
                f"Parsed path {file_path} (original path {row[path_kind]}) specified in {path_kind} does not exist"
            )
        return file_path

    def __parse_rows(self, combined_csv):
        for index, row in combined_csv.iterrows():
            sub_job = app.jobs_extension.create_new_job(
                f"Importing csv row",
                "dams.csv_row_import",
                parent_job_id=self.main_job.identifiers[0],
                user=self.user,
            )
            self.sub_jobs.append(sub_job)
            try:
                row = row.str.strip()
                self.__check_malformed_row(row)
                file_path = self.__parse_path(row)
                filename = os.path.basename(file_path)
                priref = base64.b64encode(row["Priref"].encode("ascii")).decode("ascii")
                object_id = row["Objectnummer"]
                app.jobs_extension.progress_job(sub_job, asset_id=object_id)
                upload_location = self.__get_upload_location(
                    priref, object_id, filename, row
                )
                self.__upload_file(upload_location, file_path)
                os.remove(file_path)
                app.jobs_extension.finish_job(
                    sub_job,
                    self.main_job,
                    f"Successfully imported {filename} for {object_id}",
                )
            except Exception as ex:
                if isinstance(ex, DuplicateFileException):
                    os.remove(file_path)
                message = f"Import of row {index} failed with: {ex}"
                app.jobs_extension.fail_job(sub_job, message=message)
                app.logger.error(message)

    def __post_request(self, location, content=None, file=None, raise_exception=True):
        app.logger.info(f"POST to {location}, content {content}, file {file}")
        req = requests.post(location, json=content, data=file, headers=self.headers)
        if req.status_code == 201:
            return req
        msg = f"POST to {location} with headers {req.headers}, content {content}, file {file} failed with {req.text.strip()}"
        if not raise_exception:
            app.logger.info(msg)
            return req
        elif req.status_code == 409:
            raise DuplicateFileException(msg)
        else:
            raise Exception(msg)

    def __read_csv(self, all_csv_files):
        dataframes = []
        for f in all_csv_files:
            sub_job = app.jobs_extension.create_new_job(
                f"Reading csv",
                "dams.csv_read",
                parent_job_id=self.main_job.identifiers[0],
                user=self.user,
            )
            self.sub_jobs.append(sub_job)
            try:
                df = pd.read_csv(f).dropna(how="all")
                df.columns = df.columns.str.capitalize()
                self.__validate_csv(f, df)
                dataframes.append(df)
                app.jobs_extension.finish_job(sub_job, self.main_job)
            except Exception as ex:
                message = f"Reading .csv file {f} failed with: {ex}"
                app.jobs_extension.fail_job(sub_job, message=message)
                app.logger.error(message)
        if not len(dataframes):
            raise Exception("No readable .csv file found")
        return dataframes

    def __update_asset(self, object_id, priref, row):
        asset = requests.get(
            f"{self.collection_api_url}/entities/{object_id}", headers=self.headers
        ).json()
        payload = {"identifiers": [*asset["identifiers"], *[priref, row["Priref"]]]}
        location = f"{self.collection_api_url}/entities/{object_id}"
        app.logger.info(f"PATCH to {location}, content {payload}")
        req = requests.patch(location, json=payload, headers=self.headers)
        if req.status_code != 201:
            raise Exception(
                f"PATCH to {location} with headers {req.headers}, content {payload} failed with {req.text.strip()}"
            )

    def __upload_file(self, upload_location, file_path):
        with open(file_path, "rb") as file:
            self.__post_request(upload_location, file=file)

    def __validate_csv(self, file, df):
        missing = [x["key"] for x in self.mapped_fields if x["key"] not in df.columns]
        if missing:
            raise Exception(f"{file} does not include column(s) {missing}")

    def import_from_csv(self):
        self.main_job = app.jobs_extension.create_new_job(
            "Starting csv import", "dams.csv_import", user=self.user
        )
        try:
            self.selected_folder = os.path.join(self.selected_folder, "")
            all_csv_files = [
                i for i in glob.glob(self.selected_folder + "**/*.csv", recursive=True)
            ]
            app.jobs_extension.progress_job(
                self.main_job, amount_of_jobs=len(all_csv_files)
            )
            dataframes = self.__read_csv(all_csv_files)
            combined_csv = pd.concat(dataframes)
            app.jobs_extension.progress_job(
                self.main_job, amount_of_jobs=len(all_csv_files) + len(combined_csv)
            )
            self.__parse_rows(combined_csv)
            if any(x.status == Status.FAILED.value for x in self.sub_jobs):
                app.jobs_extension.fail_job(
                    self.main_job, message="One or more sub-jobs failed"
                )
            else:
                app.jobs_extension.finish_job(
                    self.main_job, message="Import successful"
                )
        except Exception as ex:
            message = f"Starting import failed with: {ex}"
            app.jobs_extension.fail_job(self.main_job, message=message)
            app.logger.error(message)
